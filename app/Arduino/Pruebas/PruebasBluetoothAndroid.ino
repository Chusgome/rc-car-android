//#include "DHT.h"
//#define DHTPIN 7
//#define DHTTYPE DHT11
//DHT dht(DHTPIN, DHTTYPE);

#include <LEANTEC_ControlMotor.h>

// Configuramos los pines que vamos a usar
ControlMotor control(2,3,7,4,5,6);
int MotorDer1=2;//El pin 2 de arduino se conecta con el pin In1 del L298N
int MotorDer2=3;//El pin 3 de arduino se conecta con el pin In2 del L298N
int MotorIzq1=7;//El pin 7 de arduino se conecta con el pin In3 del L298N
int MotorIzq2=4;//El pin 4 de arduino se conecta con el pin In4 del L298N
int PWM_Derecho=5;//El pin 5 de arduino se conecta con el pin EnA del L298N
int PWM_Izquierdo=6;//El pin 6 de arduino se conecta con el pin EnB del L298N

int ledRojo = 12; // Led Rojo cuando está parado o frenado.
int ledVerde = 13; // Led verde cuando está en movimiento. Parpaderá una vez que reciba una señal y luego volverá a estar fijo.

float voltageValue[4] = {0,0,0,0};
char inbyte = 0; //Char para leer el led

void setup() {
  // initialise serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(ledVerde, OUTPUT);
  pinMode(ledRojo, OUTPUT);
  digitalWrite(ledVerde, HIGH);
  digitalWrite(ledRojo, HIGH);
  //dht.begin();

  //Configuramos los pines como salida
  pinMode(MotorDer1, OUTPUT);
  pinMode(MotorDer2, OUTPUT);
  pinMode(MotorIzq1, OUTPUT);
  pinMode(MotorIzq2, OUTPUT);
  pinMode(PWM_Derecho, OUTPUT);
  pinMode(PWM_Izquierdo, OUTPUT);
}

/*MÉTODOS MOTORES*/
void derecha_antihorario_izquierda_horario(){
/*En esta fución la rueda derecha girará en sentido antihorario y la
izquierda en sentido horario.
En este caso, si las ruedas estuvieran en el chasis de un robot, el
robot retrocederia.*/
  digitalWrite(MotorDer1,HIGH);
  digitalWrite(MotorDer2,LOW);
  digitalWrite(MotorIzq1,HIGH);
  digitalWrite(MotorIzq2,LOW);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void derecha_horario_izquierda_antihorario(){
/*En esta fución la rueda derecha girará en sentido horario y la
izquierda en sentido antihorario.
En este caso, si las ruedas estuvieran en el chasis de un robot, el
robot avanzaría.*/
  digitalWrite(MotorDer1,LOW);
  digitalWrite(MotorDer2,HIGH);
  digitalWrite(MotorIzq1,LOW);
  digitalWrite(MotorIzq2,HIGH);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void giro_horario(){
/*En esta fución ambas ruedas girarán en sentido horario.
En este caso, si las ruedas estuvieran en el chasis de un robot, el
robot giraria a la derecha.*/
  digitalWrite(MotorDer1,HIGH);
  digitalWrite(MotorDer2,LOW);
  digitalWrite(MotorIzq1,LOW);
  digitalWrite(MotorIzq2,HIGH);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void giro_antihorario(){
/*En esta fución ambas ruedas girarán en sentido antihorario.
En este caso, si las ruedas estuvieran en el chasis de un robot, el
robot giraria a la izquierda.*/
  digitalWrite(MotorDer1,LOW);
  digitalWrite(MotorDer2,HIGH);
  digitalWrite(MotorIzq1,HIGH);
  digitalWrite(MotorIzq2,LOW);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void parar(){
/*Función para que las ruedas paren*/
  digitalWrite(MotorDer1,LOW);
  digitalWrite(MotorDer2,LOW);
  digitalWrite(MotorIzq1,LOW);
  digitalWrite(MotorIzq2,LOW);
  analogWrite(PWM_Derecho,0);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,0);//Velocidad motor izquierdo 200

}


void pruebas (){
/*Realiza todo los métodos de comprobación de los motores*/
giro_horario();//Llamamos a la función giro_horario
delay(3000);//Durante 3 segundos ejecutamos esa función
giro_antihorario();//Llamamos a la función giro_antihorario
delay(3000);//Durante 3 segundos ejecutamos esa función
derecha_horario_izquierda_antihorario();//Llamamos a la función derecha_horario_izquierda_antihorario
delay(3000);//Durante 3 segundos ejecutamos esa función
derecha_antihorario_izquierda_horario();//Llamamos a la función derecha_antihorario_izquierda_horario
delay(3000);//Durante 3 segundos ejecutamos esa función
parar();//Llamamos a la función parar
delay(3000);//Durante 3 segundos ejecutamos esa función
}



void loop() {

  if (Serial.available() > 0)
  {
    inbyte = Serial.read();
    Serial.println(inbyte);
    if (inbyte == '1'){/*atras*/
      derecha_antihorario_izquierda_horario();
    }
    if (inbyte == '2'){/*delante*/
      derecha_horario_izquierda_antihorario();
    }
    if (inbyte == '3'){/*derecha*/
      giro_horario();
    }
    if (inbyte == '4'){/*izquierda*/
      giro_antihorario();
    }
    if (inbyte == '5'){/*test*/
      pruebas();
    }
    if (inbyte == '6'){/*luces*/
      digitalWrite(ledVerde, HIGH);
    }
    if (inbyte == '7'){/*parar*/
      parar();
      digitalWrite(ledVerde, LOW);
    }

  }
  sendAndroidValues();
  delay(500);
}


//enviar los valores por el dipositivo android por el modulo Bluetooth
void sendAndroidValues()
 {
  Serial.print('#'); //hay que poner # para el comienzo de los datos, así Android sabe que empieza el String de datos
  for(int k=0; k<4; k++)
  {
    Serial.print(voltageValue[k]);
    Serial.print('+'); //separamos los datos con el +, así no es más fácil debuggear la información que enviamos
  }
 Serial.print('~'); //con esto damos a conocer la finalización del String de datos
 Serial.println();
 delay(10);        //agregamos este delay para eliminar tramisiones faltantes
}








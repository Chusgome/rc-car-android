*CONFIGURACIÓN DEL MÓDULO ESP8266 NodeMCU V3

- Añadir las librerísa JSON al compilador, en este caso Arduino desde Archivo/Preferencias.
    #Gestor de URLs adicionales de Tarjetas:
    (http://arduino.esp8266.com/stable/package_esp8266com_index.json)

- Utilizar la versión de NodeMCU 1.0 (ESP 12E - Module) desde Herramientas/Placa en el compilador Arduino.

*CONFIGURACIÓN DEL MÓDULO HC-05

- Contraseña por defecto: 1234

*CONFIGURACIÓN DEL MÓDULO L298N

- Añadir las librerías (LEANTEC ControlMotor) al compilador de Arduino, en este caso desde Programa/Incluir librería.
    #Añadir librería ZIP:
    (https://www.leantec.es/index.php?controller=attachment&id_attachment=36)
    #Programa/Incluir librería/LEANTEC ControlMotor
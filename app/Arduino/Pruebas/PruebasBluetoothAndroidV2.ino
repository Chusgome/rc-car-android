//#include "DHT.h"
//#define DHTPIN 7
//#define DHTTYPE DHT11
//DHT dht(DHTPIN, DHTTYPE);

#include <LEANTEC_ControlMotor.h>

// Configuramos los pines que vamos a usar
ControlMotor control(2,3,7,4,5,6);
int MotorDer1=2;//El pin 2 de arduino se conecta con el pin In1 del L298N
int MotorDer2=3;//El pin 3 de arduino se conecta con el pin In2 del L298N
int MotorIzq1=7;//El pin 7 de arduino se conecta con el pin In3 del L298N
int MotorIzq2=4;//El pin 4 de arduino se conecta con el pin In4 del L298N
int PWM_Derecho=5;//El pin 5 de arduino se conecta con el pin EnA del L298N
int PWM_Izquierdo=6;//El pin 6 de arduino se conecta con el pin EnB del L298N

int ledVerde = 13; // Luces del coche
//Sensor de proximidad
int trigPin = 12;
int echoPin = 11;

//int led 2; Alerta de proximidad ( Pruebas con las luces ya definidas ).

char inbyte = 0; //Char para leer la información recibida.

bool iniciar = false;

void setup() {
  // initialise serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(ledVerde, OUTPUT);
  //digitalWrite(ledVerde, HIGH);
  //dht.begin();

  //Configuramos los pines del motor como salida
  pinMode(MotorDer1, OUTPUT);
  pinMode(MotorDer2, OUTPUT);
  pinMode(MotorIzq1, OUTPUT);
  pinMode(MotorIzq2, OUTPUT);
  pinMode(PWM_Derecho, OUTPUT);
  pinMode(PWM_Izquierdo, OUTPUT);

  //SENSOR PROXIMIDAD
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(ledVerde, OUTPUT);
}


/*MËTODO SENSOR PROXIMIDAD*/
void recogerDatosSensor () {
  if(iniciar){
    long t; //timepo que demora en llegar el eco
    long d; //distancia en centimetros

    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);//Enviamos un pulso de 10us
    digitalWrite(trigPin, LOW);

    t = pulseIn(echoPin, HIGH);//obtenemos el ancho del pulso
    d = t/59;//escalamos el tiempo a una distancia en cm

    Serial.print("Distancia: ");
    Serial.print(d);//Enviamos serialmente el valor de la distancia
    if(d<80){
      digitalWrite(ledVerde, HIGH);
      delay(100);
      digitalWrite(ledVerde, LOW);
    }
    if(d<=60) {
      parar();
    }
    Serial.print("cm");
    Serial.println();
    delay(100);//Hacemos una pausa de 100ms
  }
}

/*MÉTODOS MOTORES*/
void atras(){
  digitalWrite(MotorDer1,HIGH);
  digitalWrite(MotorDer2,LOW);
  digitalWrite(MotorIzq1,HIGH);
  digitalWrite(MotorIzq2,LOW);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void delante(){
  digitalWrite(MotorDer1,LOW);
  digitalWrite(MotorDer2,HIGH);
  digitalWrite(MotorIzq1,LOW);
  digitalWrite(MotorIzq2,HIGH);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void derecha(){
  digitalWrite(MotorDer1,HIGH);
  digitalWrite(MotorDer2,LOW);
  digitalWrite(MotorIzq1,LOW);
  digitalWrite(MotorIzq2,HIGH);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void izquierda(){
  digitalWrite(MotorDer1,LOW);
  digitalWrite(MotorDer2,HIGH);
  digitalWrite(MotorIzq1,HIGH);
  digitalWrite(MotorIzq2,LOW);
  analogWrite(PWM_Derecho,500);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,500);//Velocidad motor izquierdo 200

}
void parar(){
/*Función para que las ruedas paren*/
  digitalWrite(MotorDer1,LOW);
  digitalWrite(MotorDer2,LOW);
  digitalWrite(MotorIzq1,LOW);
  digitalWrite(MotorIzq2,LOW);
  analogWrite(PWM_Derecho,0);//Velocidad motor derecho 200
  analogWrite(PWM_Izquierdo,0);//Velocidad motor izquierdo 200

}


void pruebas (){
/*Realiza todo los métodos de comprobación de los motores*/
derecha();
delay(2000);
izquierda();
delay(2000);
delante();
delay(2000);
atras();
delay(2000);
parar();
delay(2000);
}


void loop() {
  recogerDatosSensor();

  if (Serial.available() > 0){
    inbyte = Serial.read();
    Serial.println(inbyte);
    if (inbyte == '1'){/*atras*/
      atras();
    }
    if (inbyte == '2'){/*delante*/
      delante();
    }
    if (inbyte == '3'){/*izquierda*/
      izquierda();
    }
    if (inbyte == '4'){/*derecha*/
      derecha();
    }
    if (inbyte == '5'){/*test*/
      pruebas();
    }
    if (inbyte == '6'){/*luces on*/
      digitalWrite(ledVerde, HIGH);
    }
    if (inbyte == '7'){/*parar*/
      parar();
      digitalWrite(ledVerde, LOW);
    }
    if(inbyte == '8') {/*sensor on*/
      iniciar = true;
    }
    if(inbyte == '9') {/*luces off*/
      digitalWrite(ledVerde, LOW);
    }
    if(inbyte == 'a') {/*sensor off*/
      iniciar = false;
    }

  }
  sendAndroidValues();
  delay(500);
}


//enviar los valores por el dipositivo android por el modulo Bluetooth
void sendAndroidValues()
 {
  Serial.print('#'); //hay que poner # para el comienzo de los datos, así Android sabe que empieza el String de datos
  for(int k=0; k<4; k++){
    Serial.print('+'); //separamos los datos con el +, así no es más fácil debuggear la información que enviamos
  }
  Serial.print('~'); //con esto damos a conocer la finalización del String de datos
  Serial.println();
  delay(10);        //agregamos este delay para eliminar tramisiones faltantes
}

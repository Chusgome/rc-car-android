// Conectamos con la base de datos creada en Firebase.
var dbRef = new Firebase('https://wifi-geolocolizacion-arduino.firebaseio.com/'); 
var costumersRef = dbRef.child('dispositivo'); 
var markers = {} 

// Cargamos.
costumersRef.on("child_added", function(snap) {

// Mostramos la localización en el mapa.
 addNewLoc(snap.val().lat,snap.val().lon,snap.val().nombre); 
}); 

//Modificamos.
costumersRef.on("child_changed", function (snap) { 
 changeLoc(snap.val().lat, snap.val().lon, snap.val().nombre); 
}); 
 
// Iniciamos el mapa.
var map; 
function initMap() { 
 // Center map 
 var myLatLng = {lat: 38.392101, lng: -0.525467}; 
 map = new google.maps.Map(document.getElementById('map'), { 
   center: myLatLng, 
   zoom: 6
 }); 
} 
 
// Funciones de añadir/cambiar localización.
function addNewLoc(lat, lon,nombre){ 
 var marker = new google.maps.Marker({ 
   position: new google.maps.LatLng(lat,lon), 
   map: map, 
   title: nombre // Tooltip = MAC address 
 }); 
  markers[key]; 
} 
 
function changeLoc(lat, lon, nombre) { 
   var marker = markers[key]; 
   marker = new google.maps.Marker({ 
       position: new google.maps.LatLng(lat, lon), 
       map: map, 
       title: nombre // Tooltip = MAC address 
   }); 
   markers[key] = marker; 
}
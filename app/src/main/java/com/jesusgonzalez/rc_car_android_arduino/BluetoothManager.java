package com.jesusgonzalez.rc_car_android_arduino;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothManager extends AppCompatActivity {

    private ImageButton btnUp, btnDown, btnLeft, btnRight;
    private Button btnTest, btnLights, btnStop, btnSensor;
    private TextView tvDatosRecibidos, tvSizeString;
    private Handler bluetoothIn;
    private final int handlerState = 0; //Usado para identificar el mensaje del Handler.
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();
    private ConnectedThread mConnectedThread;
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // Servicio SPP UUID - Valor por defecto para la mayoría de dispositivos.
    private boolean luces =  false;
    private boolean sensor = false;

    //Dirección MAC.
    private static String address = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_manager);

        //Declarando los botones y asociandole sus funciones.
        btnUp = findViewById(R.id.btnUp);
        btnDown = findViewById(R.id.btnDown);
        btnLeft = findViewById(R.id.btnLeft);
        btnRight = findViewById(R.id.btnRight);
        btnTest = findViewById(R.id.btnTest);
        btnLights = findViewById(R.id.btnLights);
        btnStop = findViewById(R.id.btnStop);
        btnSensor = findViewById(R.id.btnSensor);
        tvDatosRecibidos = findViewById(R.id.tvDatosRecibidos);
        tvSizeString = findViewById(R.id.tvSizeString);

        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {//if message is what we want
                    String readMessage = (String) msg.obj;// msg.arg1 = bytes from connect thread
                    recDataString.append(readMessage);//keep appending to string until ~
                    int endOfLineIndex = recDataString.indexOf("~");// determine the end-of-line
                    if (endOfLineIndex > 0) { // make sure there data before ~
                        String dataInPrint = recDataString.substring(0, endOfLineIndex);// extract string
                        tvDatosRecibidos.setText("Datos recibidos = " + dataInPrint);
                        int dataLength = dataInPrint.length();//get length of data received
                        tvSizeString.setText("Tamaño del String = " + String.valueOf(dataLength));

                        recDataString.delete(0, recDataString.length());//clear all string data
                        // strIncom =" ";
                        dataInPrint = " ";
                    }
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();

        //Mandamos la información a arduino.
        btnUp.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("1"); /*Movimiento hacía adelante.*/
            }
        });

        btnDown.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("2"); /*Movimiento hacía detrás.*/
            }
        });

        btnLeft.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("3"); /*Movimiento hacía la derecha.*/
            }
        });

        btnRight.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("4"); /*Movimiento hacía la izquierda*/
            }
        });

        btnTest.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("5"); /*Lanzamos pruebas del coche*/
            }
        });

        btnLights.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if(!luces){
                    mConnectedThread.write("6"); /*Encendido de luces*/
                    luces = true;
                } else {
                    mConnectedThread.write("9"); /*Apagado de luces*/
                    luces = false;
                }
            }
        });

        btnStop.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("7"); /*Paramos el coche*/
            }
        });

        btnSensor.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if(!sensor){
                    mConnectedThread.write("8"); /*Encendido de sensor*/
                    sensor = true;
                } else {
                    mConnectedThread.write("a"); /*Apagado de sensor*/
                    sensor = false;
                }
            }
        });

    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = getIntent();
        //Recogemos la dirección MAC.
        address = intent.getStringExtra(BluetoothDeviceList.EXTRA_DEVICE_ADDRESS);

        //Creamos un adaptador y le pasamos la dirección MAC recogida.
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
        }
        //Establecemos la conexión.
        try {
            btSocket.connect();
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
        mConnectedThread.write("x");
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            //Cerramos el socket cuando no se utilice.
            btSocket.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    //Comprobará el estado del bluetooth en el dispositivo. Si no esta conectado, te pedirá si puede habilitarlo.
    private void checkBTState() {
        if (btAdapter == null) {
            Toast.makeText(getBaseContext(), "Este dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer); //Leemos los datos.
                    String readMessage = new String(buffer, 0, bytes);
                    //Mandamos los datos obtenidos a la actividad via Handler.
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();//Cogemos la cadena recogida y la pasamos a bytes.
            try {
                mmOutStream.write(msgBuffer);//Escribimos los datos.
            } catch (IOException e) {
                Toast.makeText(getBaseContext(), "La conexión con el adaptador ha fallado", Toast.LENGTH_LONG).show();
                finish();

            }
        }

    }


}


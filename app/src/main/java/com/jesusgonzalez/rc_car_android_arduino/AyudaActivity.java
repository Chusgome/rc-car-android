package com.jesusgonzalez.rc_car_android_arduino;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Random;

public class AyudaActivity extends AppCompatActivity {

    private EditText etEmail;
    private EditText etMensaje;
    private Button btEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda);

        etEmail = findViewById(R.id.etEmail);
        etMensaje = findViewById(R.id.etMensaje);
        btEnviar = findViewById(R.id.btEnviarEmail);
        btEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMail();
                Snackbar.make(getCurrentFocus(), "CORREO ENVIADO A SOPORTE", Snackbar.LENGTH_LONG).show();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        cleanEditText();
                        finish();
                    }
                };
                handler.postDelayed(runnable, 2000);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void sendMail() {
        Random rnd = new Random();
        int aleatorio = rnd.nextInt(1000);
        new Email().sendEmail(etEmail.getText().toString(), "SOPORTE TÉCNICO DROID CAR - REF0" + aleatorio,
                "Bienvenido al soporte.<br/>" +
                        "Su número de soporte es: 0" + aleatorio + "<br/>" +
                        "Nos pondremos en contacto con usted próximamente en un plazo de 24/72 horas.<br/><br/>" +
                        "Un saludo, el Departamanteo de Droid Car.");
        new Email().sendEmail("droidcartest@gmail.com", "SOPORTE TÉCNICO DROID CAR - REF0" + aleatorio,
                etMensaje.getText().toString() + "<br/><br/>" +
                        "Email de contacto: " + etEmail.getText().toString());
    }

    private void cleanEditText() {
        etEmail.setText("");
        etMensaje.setText("");
    }
}

package com.jesusgonzalez.rc_car_android_arduino;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;


public class BluetoothDeviceList extends Activity {

    private static final String TAG = "DeviceListActivity";

    TextView tvConexion;

    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    private BluetoothAdapter mBtAdapter;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkBTState();

        tvConexion = (TextView) findViewById(R.id.connecting);
        tvConexion.setTextSize(12);
        tvConexion.setText(" ");

        //Iniciamos el arrayAdapter.
        mPairedDevicesArrayAdapter = new ArrayAdapter<>(this, R.layout.device_name);

        //Rellenamos la lista asignandole el adaptador creado previamente.
        ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        //Recogemos el adaptador de bluetooth.
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        if(mBtAdapter!=null) {
            //Recogemos los dispositivos ya enlazados.
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);//make title viewable
                for (BluetoothDevice device : pairedDevices) {
                    mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            } else {
                String noDevices = getResources().getText(R.string.none_paired).toString();
                mPairedDevicesArrayAdapter.add(noDevices);
            }
        }
    }

    private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            tvConexion.setText("CONECTANDO..");
            //Recogemos la MAC.
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            //Si la conexión es correcta, pasaremos a la actividad de control del coche RC.
            Intent intent = new Intent(BluetoothDeviceList.this, BluetoothManager.class);
            intent.putExtra(EXTRA_DEVICE_ADDRESS, address);
            startActivity(intent);
        }
    };

    private void checkBTState() {
        //Comprobamos el estado del bluetooth. Si está activado o no.
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta Bluetooth", Toast.LENGTH_LONG).show();
            finish();
        } else {
            if (mBtAdapter.isEnabled()) {
                Log.d(TAG, "Bluetooth Activado");
            } else {
                //En caso de que no este activado, lanzamos la ventana para activarlo. En caso de darle a cancelar, volverá a principal.
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }
    }
}
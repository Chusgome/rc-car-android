package com.jesusgonzalez.rc_car_android_arduino;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LocalizacionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localizacion);

        loadWebView();
    }


    private void loadWebView() {
        WebView webView = findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //Alternativas
        //webView.loadUrl("http://192.168.1.132/www/localizacion_mapa.html");/*Servidor Local*/
        webView.loadUrl("http://80.211.226.74/localizacion_mapa.html");/*Servidor Remoto 001*/


    }
}

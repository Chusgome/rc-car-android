package com.jesusgonzalez.rc_car_android_arduino;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class Email {

    private static final String SMTP_HOST_NAME = "smtp.gmail.com"; //HOST Gmail
    private static final String SMTP_AUTH_USER = "droidcartest@gmail.com"; //Dirección de correo
    private static final String SMTP_AUTH_PWD = "DroidCar*1"; //Contraseña del correo

    private static Message message;


    public static void sendEmail(String to, String subject, String msg) {
        String from = "droidcartest@gmail.com"; //Remitente

        final String username = SMTP_AUTH_USER;
        final String password = SMTP_AUTH_PWD;

        String host = SMTP_HOST_NAME;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        //Recogemos la sesión
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            //MimeMessage por defecto.
            message = new MimeMessage(session);

            //Rremitente.
            message.setFrom(new InternetAddress(from));

            //El/los destinatarios.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            //Asunto.
            message.setSubject(subject);

            //Mensaje,
            BodyPart messageBodyPart = new MimeBodyPart();

            //Tipo de contenido para el mensaje.
            messageBodyPart.setContent(msg, "text/html");

            //Componemos el email.
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            //Enviamos el mensaje completo.
            message.setContent(multipart);

            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Transport.send(message);
                        System.out.println("Email enviado correctamente.");
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

            thread.start();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
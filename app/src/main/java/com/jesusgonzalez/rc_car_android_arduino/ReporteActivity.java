package com.jesusgonzalez.rc_car_android_arduino;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Random;

public class ReporteActivity extends AppCompatActivity {

    private Button btEnviar;
    private EditText etMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);

        btEnviar = findViewById(R.id.btnEnviarIncidencia);
        etMensaje = findViewById(R.id.etMensajeIncidencia);

        btEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendReporte(view);
                clear();
            }
        });

    }


    public void sendReporte(View view) {
        sendMail();
        Snackbar.make(view, "INCIDENCIA REGISTRADA CORRECTAMENTE. GRACIAS", Snackbar.LENGTH_LONG).show();
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };
        handler.postDelayed(runnable, 2000);
    }

    private void clear(){
        etMensaje.setText("");
    }

    public void sendMail() {
        Random rnd = new Random();
        int aleatorio = rnd.nextInt(1000);
        new Email().sendEmail("droidcartest@gmail.com", "SOPORTE INCIDENCIAS DROID CAR - REF0" + aleatorio,
                etMensaje.getText().toString());
    }
}

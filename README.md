# RC - CAR - ANDROID - ARDUINO

Este proyecto está dedicado al control de un coche totalmente domotizado a través de una aplicación android mediante un módulo bluetooth.

## Inicialización

rc-car-android-arduino (Aplicación principal).

## Requisitos

Módulo Arduino UNO.

Módulo HC-05.

Prototipo de Coche RC.

Dispositivo Android.

## Instalación

Descargar la aplicación.

Montaje del prototipo del coche RC con todos sus componentes.

Comunicar dispositivo android con el módulo bluetooth.

## Pruebas

Arrancar la aplicación android y los proyectos pertinentes de prueba adjuntos en el proyecto.


## Deployment

Descargar la 'APK' e instalarla en el dispositibo android.

Subir el código mediante el compilador a nuestro Arduino UNO.

## Montaje de la aplicación

* [EJEMPLO](http://www.dropwizard.io/1.0.2/docs/) - Recursos utilizados


#### CONFIGURACIÓN DEL MÓDULO ESP8266 NodeMCU V3

    - Añadir las librerísa JSON al compilador, en este caso Arduino desde Archivo/Preferencias.
        #Gestor de URLs adicionales de Tarjetas: (http://arduino.esp8266.com/stable/package_esp8266com_index.json)
        

    - Utilizar la versión de NodeMCU 1.0 (ESP 12E - Module) desde Herramientas/Placa en el compilador Arduino.

    
#### CONFIGURACIÓN DEL MÓDULO HC-05

        - Contraseña por defecto: 1234

#### CONFIGURACIÓN DEL MÓDULO L298N

        - Añadir las librerías (LEANTEC ControlMotor) al compilador de Arduino, en este caso desde Programa/Incluir librería.
            #Añadir librería ZIP: (https://www.leantec.es/index.php?controller=attachment&id_attachment=36)
            #Programa/Incluir librería/LEANTEC ControlMotor

## Contribuciones

Todas las contribuciones aportadas por Jesús González

## Versiones

v 1.0.3

Uso de [SemVer](http://semver.org/) para versiones. Mira: [tags on this repository](https://github.com/your/project/tags). 

## Autores

* **Jesús González** - *Trabajo Inicial* - [Chusgome](https://github.com/Chusgome).

Algunos contribuidores [contribuidores](https://gitlab.com/Chusgome/rc-car-android/contributors) que han participado en este proyeto.

## Licencia

Este proyecto esta bajo la licencia de MIT License - Mira el fichero [LICENSE.md](LICENSE.md) para más detalles.

## Agradecimientos

* Al código reutilizado por otros programadores.
* Inspiración.
* Innovación.
* Creatividad.
* Tutoriales